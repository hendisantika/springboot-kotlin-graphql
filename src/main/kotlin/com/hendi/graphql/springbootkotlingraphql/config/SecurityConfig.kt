package com.hendi.graphql.springbootkotlingraphql.config

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-kotlin-graphql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-01
 * Time: 21:29
 */
//@Configuration
//@EnableResourceServer
//class SecurityConfig : ResourceServerConfigurerAdapter() {
//
//    @Value("\${security.oauth2.resource.id}")
//    private lateinit var resourceId: String
//
//    @Throws(Exception::class)
//    override fun configure(http: HttpSecurity) {
//        http.authorizeRequests()
//                .mvcMatchers("/graphql").authenticated()
//                .anyRequest().permitAll()
//    }
//
//    @Throws(Exception::class)
//    override fun configure(resources: ResourceServerSecurityConfigurer) {
//        resources.resourceId(resourceId)
//    }
//}