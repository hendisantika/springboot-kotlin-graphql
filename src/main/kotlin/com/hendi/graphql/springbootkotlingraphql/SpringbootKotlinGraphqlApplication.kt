package com.hendi.graphql.springbootkotlingraphql

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringbootKotlinGraphqlApplication

fun main(args: Array<String>) {
    runApplication<SpringbootKotlinGraphqlApplication>(*args)
}
