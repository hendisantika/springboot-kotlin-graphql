package com.hendi.graphql.springbootkotlingraphql.resolver

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.hendi.graphql.springbootkotlingraphql.entity.Review
import com.hendi.graphql.springbootkotlingraphql.repository.ReviewRepository
import org.springframework.stereotype.Component

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-kotlin-graphql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-01
 * Time: 21:30
 */
@Component
class ReviewMutationResolver(private val reviewRepository: ReviewRepository) : GraphQLMutationResolver {
    fun newReview(snackId: String, rating: Int, text: String): Review {
        val review = Review(snackId, rating, text)
        reviewRepository.save(review)
        return review
    }
}