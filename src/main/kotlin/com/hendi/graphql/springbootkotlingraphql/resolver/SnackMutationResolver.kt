package com.hendi.graphql.springbootkotlingraphql.resolver

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.hendi.graphql.springbootkotlingraphql.entity.Snack
import com.hendi.graphql.springbootkotlingraphql.repository.SnackRepository
import org.springframework.stereotype.Component
import java.util.*

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-kotlin-graphql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-02
 * Time: 07:02
 */
@Component
class SnackMutationResolver(private val snackRepository: SnackRepository) : GraphQLMutationResolver {
    fun newSnack(name: String, amount: Float): Snack {
        val snack = Snack(name, amount)
        snack.id = UUID.randomUUID().toString()
        snackRepository.save(snack)
        return snack
    }

    fun deleteSnack(id: String): Boolean {
        snackRepository.deleteById(id)
        return true
    }

    fun updateSnack(id: String, amount: Float): Snack {
        val snack = snackRepository.findById(id)
        snack.ifPresent {
            it.amount = amount
            snackRepository.save(it)
        }
        return snack.get()
    }
}

