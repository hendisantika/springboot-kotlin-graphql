package com.hendi.graphql.springbootkotlingraphql.resolver

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.hendi.graphql.springbootkotlingraphql.entity.Review
import com.hendi.graphql.springbootkotlingraphql.entity.Snack
import com.hendi.graphql.springbootkotlingraphql.repository.SnackRepository
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.stereotype.Component

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-kotlin-graphql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-02
 * Time: 07:02
 */
@Component
class SnackQueryResolver(val snackRepository: SnackRepository,
                         private val mongoOperations: MongoOperations) : GraphQLQueryResolver {
    fun snacks(): List<Snack> {
        val list = snackRepository.findAll()
        for (item in list) {
            item.reviews = getReviews(snackId = item.id)
        }
        return list
    }

    private fun getReviews(snackId: String): List<Review> {
        val query = Query()
        query.addCriteria(Criteria.where("snackId").`is`(snackId))
        return mongoOperations.find(query, Review::class.java)
    }
}