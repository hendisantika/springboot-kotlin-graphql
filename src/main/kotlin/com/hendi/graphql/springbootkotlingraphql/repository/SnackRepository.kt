package com.hendi.graphql.springbootkotlingraphql.repository

import com.hendi.graphql.springbootkotlingraphql.entity.Snack
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-kotlin-graphql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-01
 * Time: 21:27
 */
@Repository
interface SnackRepository : MongoRepository<Snack, String>