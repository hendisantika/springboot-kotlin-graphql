package com.hendi.graphql.springbootkotlingraphql.entity

import org.springframework.data.mongodb.core.mapping.Document

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-kotlin-graphql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-01
 * Time: 21:24
 */
@Document(collection = "reviews")
data class Review(
        var snackId: String,
        var rating: Int,
        var text: String
)