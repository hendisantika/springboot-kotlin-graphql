# Building GraphQL APIs with Kotlin, Spring Boot, and MongoDB

In this article, you will learn how to build GraphQL APIs with Kotlin, Spring Boot, and MongoDB. Also, as you wouldn't want to publish unsecure APIs, you will learn how to secure this stack with Auth0.

## Things to do:
1. Clone the repository: 
    ```
    git clone https://gitlab.com/hendisantika/springboot-kotlin-graphql.git
    ````
2. Go to the folder: `cd springboot-kotlin-graphql`
3. Run the app: `gradle clean bootRun --info`
4. Open your favorite browser: http://localhost:8080/graphiql

## Image Screen shot

Home Page

![GraphiQL Home Page](img/home.png "GraphiQL Home Page")

On that application, you can use a mutation to add a newSnack. To see this in action, copy and paste the following code into the left-hand side panel and click on the play button (or hit Ctrl + Enter in your keyboard):
Home Page

![GraphiQL Mutation Page](img/mutation.png "GraphiQL Mutation Page")

```
mutation {
  newSnack(name: "French Fries", amount: 40.5) {
    id
    name
    amount
  }
}
```
If everything runs as expected, you will get the following result back:
```
{
  "data": {
    "newSnack": {
      "id": "da84885b-b160-4c09-a5ea-3484bac4d5f9",
      "name": "French Fries",
      "amount": 40.5
    }
  }
}
```

You just created a new snack. Awesome, right? Now, you can create a review for this snack:
Add New Review Page

![Add New Review Page](img/review.png "Add New Review Page")

```
mutation {
    newReview(snackId:"SNACK_ID",
    text: "Awesome snack!", rating:5
    ){
        snackId, text, rating
    }
}
```
> Note: You will have to replace SNACK_ID with the id returned for your new snack on the previous command.

Running this command will result in the following response:
```
{
  "data": {
    "newReview": {
      "snackId": "da84885b-b160-4c09-a5ea-3484bac4d5f9",
      "text": "Awesome snack!",
      "rating": 5
    }
  }
}
```

Now, to fetch the snacks and reviews persisted in your database, you can issue the following query:
Fetch the snacks and reviews Page

![fetch the snacks and reviews Page](img/snack.png "fetch the snacks and reviews page")

```
query {
  snacks {
    name,
    reviews {
      text, rating
    }
  }
}
```
Running this query will get you back a response similar to this:
```
{
  "data": {
    "snacks": [
      {
        "name": "French Fries",
        "reviews": [
          {
            "text": "Awesome snack!",
            "rating": 5
          }
        ]
      }
    ]
  }
}
```

This is the beauty of GraphQL. With just one query you can decide what is the exact format you need for the result.

> "GraphQL allows client applications to define what data they need exactly, nothing more, nothing less."

Read more at: https://auth0.com/blog/building-graphql-apis-with-kotlin-spring-boot-and-mongodb/

